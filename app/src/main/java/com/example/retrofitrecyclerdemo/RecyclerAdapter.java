package com.example.retrofitrecyclerdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{


    private ArrayList<Pokemon_> arrayListPokemon;
    private Context context;
    private View.OnClickListener clickListener;

    public RecyclerAdapter(ArrayList<Pokemon_> arrayListPokemon, Context context) {
        this.arrayListPokemon = arrayListPokemon;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleitem,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide.with(context).asBitmap().load(arrayListPokemon.get(position).getImage()).into(holder.imgv_pokemon);
        holder.txt_pokemon.setText(arrayListPokemon.get(position).getName());

    }

    public void setOnClickListner(View.OnClickListener onClickListner)
    {
        clickListener = onClickListner;
    }

    @Override
    public int getItemCount() {
        return arrayListPokemon.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
          ImageView imgv_pokemon;
          TextView txt_pokemon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgv_pokemon = itemView.findViewById(R.id.imgv_pokemon);
            txt_pokemon = itemView.findViewById(R.id.txt_pokemon);

            itemView.setTag(this);
            itemView.setOnClickListener(clickListener);

        }
    }
}
