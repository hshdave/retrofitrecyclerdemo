package com.example.retrofitrecyclerdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    ArrayList<Pokemon_> parrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<Pokemon> call = service.getAllPokemons();

        call.enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {

                System.out.println("Response From URL :" + response.body());

                try {
                    Pokemon pokemon = response.body();

                    parrayList = new ArrayList<>(pokemon.getPokemon());

                    generateView(parrayList);

                }catch (NullPointerException e)
                {
                    System.out.println("Nullpointer Exception :"+e.getMessage());
                }



            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {

                System.out.println("In Failure :" + t.getMessage());

            }
        });


    }

    public void generateView(ArrayList<Pokemon_> pokemonS)
    {
        recyclerAdapter = new RecyclerAdapter(pokemonS, getApplicationContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL,false);
        recyclerView = findViewById(R.id.main_recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setOnClickListner(onClickListener);

    }


    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) v.getTag();
            int position = viewHolder.getAdapterPosition();

            Toast.makeText(getApplicationContext(),parrayList.get(position).getName(),Toast.LENGTH_SHORT).show();

            Intent i = new Intent(MainActivity.this,Pokemondetails.class);
            i.putExtra("pokemon",parrayList.get(position));
            startActivity(i);


        }
    };
}
