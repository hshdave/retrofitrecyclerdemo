package com.example.retrofitrecyclerdemo;


import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("E14trR2lD")
    Call<Pokemon> getAllPokemons();

}
