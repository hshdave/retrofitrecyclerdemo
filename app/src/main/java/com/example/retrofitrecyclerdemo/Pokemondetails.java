package com.example.retrofitrecyclerdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class Pokemondetails extends AppCompatActivity {

    ImageView pimageView;
    TextView txt_name,txt_type,txt_ability,txt_height,txt_weight,txt_desc;
    Pokemon_  pokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemondetails);

        pimageView = findViewById(R.id.pd_imgv);
        txt_name = findViewById(R.id.pd_txtName);
        txt_type = findViewById(R.id.pd_txtType);
        txt_ability = findViewById(R.id.pd_txtAbility);
        txt_height = findViewById(R.id.pd_txtHeight);
        txt_weight = findViewById(R.id.pd_txtWeight);
        txt_desc = findViewById(R.id.pd_txtDesc);

        pokemon = getIntent().getParcelableExtra("pokemon");

        generateView();

        txt_desc.setMovementMethod(new ScrollingMovementMethod());


    }

    public void generateView()
    {
        Picasso.get().load(pokemon.getImage()).into(pimageView);
        txt_name.setText("Name :"+pokemon.getName());
        txt_type.setText("Type :"+pokemon.getType());
        txt_ability.setText("Ability :"+pokemon.getAbility());
        txt_height.setText("Height :"+pokemon.getHeight());
        txt_weight.setText("Weight :"+pokemon.getWeight());
        txt_desc.setText("Description :"+pokemon.getDescription());
    }
}
